
const es = {
  state: {
    esdata: {},
    field_num_name: '',
    pepdata:{},
    user_info: {},
    add_user_name: ''
  },
  mutations: {
    SAVE_ESBASE: (state, esdata)=>{
        state.esdata = esdata
    },
    SAVE_FIELD_NUM_NAME: (state, field_num_name)=>{
      state.field_num_name = field_num_name
    },
    SAVE_PEP_ESBASE: (state, pepdata)=>{
      state.pepdata = pepdata
    },
    SAVE_USER_INFO: (state, user_info)=>{
      state.user_info = user_info
    },
  },
  actions: {
    
  }
}

export default es

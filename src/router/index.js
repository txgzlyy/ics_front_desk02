import Vue from 'vue'
import Router from 'vue-router'
const _import = require('./_import_' + process.env.NODE_ENV)

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [
  { path: '/login', component: _import('login/index'), hidden: true },
  { path: '/404', component: _import('404'), hidden: true },
  {
    path: '/',
    component: Layout,
    // redirect: '/dashboard',
    name: '',
    icon: 'dashboard',
    hidden: true,
    // children: [{path: 'dashboard', component: _import('dashboard/index')}],
    children: [{ path: 'dashboard', component: _import('page/timingPing') }]
  },
  {
    path: '/',
    component: Layout,
    // redirect: '/dashboard',
    name: 'labs',
    icon: 'dashboard',
    hidden: true,
    // children: [{path: 'dashboard', component: _import('dashboard/index')}],
    children: [{ path: 'lab_list', component: _import('page/timingPing') }]
  },
  {
    path: '/prov_dashboard',
    name: 'data',
    component: _import('dashboard/index'),
    hidden: true
  },
  {
    path: '/ind_dashboard',
    name: 'data',
    component: _import('industry_dashboard/index'),
    hidden: true
  },
  {
    path: '/monitor',
    name: 'monitor',
    icon: 'dashboard',
    component: _import('dashboard/monitor'),
    hidden: true
  },
  {
    path: '/labs',
    name: 'labs',
    icon: 'labs',
    component: _import('labs/index'),
    children: [
      { path: 'content', name: 'vm', icon: 'content', hidden: true, component: _import('labs/content') },
      { path: 'edit', name: 'edit', icon: 'edit', hidden: true, component: _import('labs/edit') },
      { path: 'list', name: 'list', icon: 'list', hidden: true, component: _import('labs/list') },
      { path: 'poolList', name: 'poolList', icon: 'poolList', hidden: true, component: _import('labs/poolList') },
      { path: 'poolEdit', name: 'poolEdit', icon: 'poolEdit', hidden: true, component: _import('labs/poolEdit') }
    ],
    hidden: true
  },
  {
    path: '/socket',
    name: 'socket',
    icon: 'socket',
    component: _import('dashboard/socket'),
    hidden: true
  },
  {
    path: '/admin',
    component: Layout,
    icon: 'user',
    name: '用户基础服务',
    children: [
      { path: 'user', name: '用户管理', icon: 'QQ', hidden: true, component: _import('base_service/user') },
      { path: 'org', name: '> 组织管理', icon: 'QQ', component: _import('base_service/org') },
      { path: 'org_list', name: '> 组织列表', icon: 'QQ', component: _import('base_service/org_list') },
      { path: 'org_msg', name: '> 申请列表', icon: 'QQ', component: _import('base_service/msg') }
    ]
  },
  {
    path: '/statis',
    component: Layout,
    icon: 'tongji',
    name: '数据统计',
    children: [
      { path: '', name: '> 自治区，地州统计方案', icon: '', component: _import('data_statis/stat_pertj') },
      { path: 'entertj', name: '> 企业统计方案', icon: '', component: _import('data_statis/stat_list') }, // 企业角色统计设备
      { path: 'tjenter', name: '统计企业分类', icon: '', hidden: true, component: _import('data_statis/stat_tj_enter') }, // 统计企业
      { path: 'enterinfo', name: '企业分类详情', icon: '', hidden: true, component: _import('data_statis/stat_enterinfo') }, // 统计企业详情
      { path: 'tjcla', name: '统计分类', icon: '', hidden: true, component: _import('data_statis/stat_sys_tj') }, // 统计设备
      { path: 'clainfo', name: '分类详情', icon: '', hidden: true, component: _import('data_statis/stat_clainfo') }, // 统计设备详情
      { path: 'syslist', name: '> 工控子网', icon: '', component: _import('data_statis/stat_sys_list') },
      { path: 'cph', name: '综合统计表', icon: '', hidden: true, component: _import('data_statis/stat_cph_table') }
      // {path: 'alltj', name: '所有统计', icon: '', component: _import('data_statis/stat_tj')},
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

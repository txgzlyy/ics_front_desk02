//如果使用cookie可以使用js-cookie进行cookie的便捷操作
// import Cookies from 'js-cookie'
// Cookies.get('tokenKey')
// Cookies.set('tokenKey',token)

//下面采用localStorage进行token的存放
const TokenKey = 'token'

export function getToken() {
  console.log(localStorage.getItem(TokenKey))
  return localStorage.getItem(TokenKey)
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, token)
}

export function removeToken() {
  return localStorage.removeItem(TokenKey)
}

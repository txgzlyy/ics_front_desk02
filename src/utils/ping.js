import axios from 'axios'
import {Message, MessageBox} from 'element-ui'
import store from '@/store'
import {getToken} from '@/utils/auth'
import router from '@/router'

//axios.defaults.headers.post['Content-Type']='application/json';

// 创建axios实例
var fetch = axios.create({
  //baseURL: process.env.BASE_API,
  //baseURL: ''
  timeout: 15000
})

// request拦截器
fetch.interceptors.request.use(config => {
  if (store.getters.token) {
    config.headers['X-Token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})


/*fetch.interceptors.response.use(res => {
  return res;
}, err => {
  if (err.response) {
    switch (error.response.status) {
      case 401:

        // store.commit(LogOut);
         router.replace({path:'/login'})
    }
  }
});*/

export default fetch

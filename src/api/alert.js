import fetch from '@/utils/fetch'

export function GetWebSocketIoServer() {
  return fetch({
    url: '/get_web_socketio_server',
    method: 'get',
    timeout: 30000
  })
}

export function apiSetDeapartKeyType(data) {
  return fetch({
    url: '/set_depart_key_type',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function apiGetUncategorizedSyslog(count) {
  return fetch({
    url: '/get_uncategorized_syslog',
    method: 'post',
    data: {
      count: count
    }
  })

}

export function apiIgnoreDepartSyslog(row) {
  return fetch({
    url: '/ignore_depart_syslog',
    method: 'post',
    data: {
      row: row
    }
  })

}

export function apiGetSyslogClassifiData(id, count, start_time, end_time) {
  return fetch({
    url: '/get_syslog_classified_data',
    method: 'post',
    data: {
      type: "get_syslog_classified_data",
      data: {
        id: id,
        count: count,
        start_time: start_time,
        end_time: end_time
      }
    }
  })
}

export function ApiGetSyslogClassifiedType() {
  return fetch({
    url: '/get_syslog_classified_type',
    method: 'get',
  })
}

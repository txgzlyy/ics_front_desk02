import fetch from '@/utils/fetch'

export function getList(params) {
  return fetch({
    url: '/table/list',
    method: 'get',
    params
  })
}

export function getPING(domain) {
  return fetch({
    url: '/linkmon/random_ping',
    method: 'post',
    data: {
      'domain': domain
    },
    timeout: 30000,
  })
}

export function getMTR(sourceip, domain) {
  return fetch({
    url: '/linkmon/random_mtr',
    method: 'post',
    data: {
      'sourceip': sourceip,
      'domain': domain
    },
    timeout: 30000,
  })
}

export function getREGION() {
  return fetch({
    url: '/linkmon/region',
    method: 'get'
  })
}

export function getLINK() {
  return fetch({
    url: '/linkmon/update',
    method: 'post'
  })
}

export function selectIP(sourceip, domain) {
  return fetch({
    url: '/linkmon/select_by_ip',
    data: {
      'sourceip': sourceip,
      'domain': domain
    },
    method: 'post'
  })
}

export function selectTIME(sourceip, domain) {
  return fetch({
    url: '/linkmon/select_by_time',
    data: {
      'sourceip': sourceip,
      'domain': domain
    },
    method: 'post'
  })
}

export function getDEVICE() {
  return fetch({
    url: '/zabbix/basic',
    method: 'post',
    timeout: 30000
  })
}

export function getGRAPH(hostid, condition) {
  return fetch({
    url: '/zabbix/graph',
    data: {
      'hostid': hostid,
      'condition': condition 
    },
    method: 'post',
    timeout: 50000
  })
}

export function getALERT() {
  return fetch({
    url: '/zabbix/alert',
    method: 'post',
    timeout: 60000
  })
}

// export function getGROUPS(data) {
//   return fetch({
//     url: '/linkmon/get_groups_lists',
//     method: 'post',
//     data: data,
//     timeout: 30000
//   })
// }

export function getGROUPS(data) {
  var fetchInfo;
  if (data === '') {
    console.log('k')
    fetchInfo = {
      url: '/linkmon/get_groups_lists',
      method: 'get',
    }
  } else {
    console.log('n k')
    fetchInfo = {
      url: '/linkmon/get_groups_lists',
      method: 'post',
      data: {'groups': data},
    }
  }
  return fetch(fetchInfo)
}

export function newGROUP(data) {
  return fetch({
    url: '/linkmon/new_group_name',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function newGROUPIP(data) {
  return fetch({
    url: '/linkmon/new_group_ip',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function newDetected(data) {
  return fetch({
    url: 'linkmon/add_detected',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function newPoint(data) {
  return fetch({
    url: 'linkmon/add_point',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function initTask() {
  return fetch({
    url: '/linkmon/init_task',
    method: 'get',
    timeout: 30000
  })
}

export function fetchData(data) {
  return fetch({
    url: '/linkmon/get_detecting',
    method: 'post',
    data: data, 
    timeout: 30000
  })
}

export function addTask(data) {
  return fetch({
    url: '/linkmon/task',
    method: 'post',
    data: {
      source: data['detecting'],
      target: data['detected'],
      label: data['label'],
      type: data['type']
    },
    timeout: 150000
  })
}

export function startTping(data) {
  return fetch({
    url: '/linkmon/start_tping',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function delDetecting(data) {
  return fetch({
    url: '/linkmon/delete_detect',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function delDetected(data) {
  return fetch({
    url: '/linkmon/delete_detect',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function updateDetected(data) {
  return fetch({
    url: '/linkmon/update_detect',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function delTping(data) {
  return fetch({
    url: '/linkmon/delete_ptask',
    method: 'post',
    data: data,
    timeout: 30000
  })
}

export function getHistory(data) {
  return fetch({
    url: '/snmp/get_history_snmp',
    method: 'post',
    data: data,
    timeout: 300000
  })
}

export function getHistoryPublic(data) {
  return fetch({
    url: '/linkmon/get_history_public',
    method: 'post',
    data: data,
    timeout: 300000
  })
}

export function getBivariateData() {
  return fetch({
    url: '/linkmon/get_two_dimensional_data',
    method: 'get',
    timeout: 300000
  })
}
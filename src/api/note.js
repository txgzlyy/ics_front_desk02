import fetch from '@/utils/fetch'

// push data to server
export function push_note(data) {
  return fetch({
    url: '/notification_add_note',
    method: 'post',
    data: {data:data}
  })
}

// Gains data from server
export function get_notes() {
  return fetch({
    url: '/notification_get_notes',
    method: 'get'
  })
}

export function get_note(id) {
  return fetch({
    url: '/notification_get_note',
    method: 'get',
    // data:{id:id}
    params:{id:id}
  })
}

export function put_readers(id) {
  return fetch({
    url:'/notification_put_readers',
    method:'put',
    data:{id:id}
  })
}

export function delete_note(id) {
  return fetch({
    url:'/notification_delete_note',
    method:'delete',
    data:{id:id}
  })
}

export function update_note(id,data) {
  return fetch({
    url:'/notification_modify_note',
    method:'put',
    data:{id:id,data:data}
  })
}

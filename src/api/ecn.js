import fetch from '@/utils/fetch'

export function getNodes(data) {
  var fetchInfo;
  if (data === '') {
    console.log('k')
    fetchInfo = {
      url: '/ecn/get_nodes',
      method: 'get',
    }
  } else {
    console.log('n k')
    fetchInfo = {
      url: '/ecn/get_nodes',
      method: 'post',
      data: {'node': data},
    }
  }
  return fetch(fetchInfo)
}

export function newStreams(data) {
  return fetch({
    url: '/ecn/new_streams',
    method: 'post',
    data: data,
    timeout: 150000
  })
}

export function checkResource(data) {
  return fetch({
    url: '/ecn/resource_check',
    method: 'post',
    data: data,
    timeout: 150000
  })
}

export function getStreams1() {
  return fetch({
    url: '/ecn/get_streams_1',
    method: 'post'
  })
}

export function getStreams2(data) {
  return fetch({
    url: '/ecn/get_streams_2',
    method: 'post',
    data: {"custom_port": data}
  })
}

export function newNode(data) {
  return fetch({
    url: '/ecn/new_node',
    method: 'post',
    data: data
  })
}

export function getDomain(data) {
  return fetch({
    url: '/ecn/get_domain',
    method: 'post',
    data: data
  })
}

export function newMachine(data) {
  return fetch({
    url: '/ecn_w/new_machine',
    method: 'post',
    data: data
  })
}

export function newIP(data) {
  return fetch({
    url: '/ecn/new_ip',
    method: 'post',
    data: data
  })
}

export function getTraffics(date) {
  return fetch({
    url: '/ecn/get_traffics',
    method: 'post',
    data: {type: "load_traffic", daily: date}

  })
}

export function delStream(data) {
  return fetch({
    url: '/ecn/del_stream',
    method: 'post',
    data: [data]

  })
}

export function getNode(data) {
  var fetchInfo;
  if (data === '') {
    console.log('k')
    fetchInfo = {
      url: '/ecn/get_node',
      method: 'get',
    }
  }
  return fetch(fetchInfo)
}

export function getTrafficDetails(start) {
  return fetch({
    url: '/ecn/load_traffic_details',
    method: 'post',
    data: {type: "load_traffic_details", data:{limit:10,offset:start}}
  })
}

export function getAllTraffic(start_row_key) {
  return fetch({
    url: '/ecn/load_all_traffic',
    method: 'post',
    data: {"start_row_key": start_row_key}
  })
}
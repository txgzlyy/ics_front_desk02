import fetch from '@/utils/fetch'

// 获取角色
export function getEsRole() {
  return fetch({
    url: '/es_service_es_role',
    method: 'get'
  })
}

// 获得当前用户角色
export function getCruteRole(org_name) {
  return fetch({
    url: '/es_service_crute_role?org_name='+org_name,
    method: 'get'
  })
}

// 根据角色添加功能
export function inputRoleFunc(data) {
  return fetch({
    url: '/es_service_input_role_permission',
    method: 'post',
    data: data
  })
}

// 自定义自由表格
export function createBase(data) {
  return fetch({
    url: '/es_service_create_base',
    method: 'post',
    data: data
  })
}

// 修改自由表格
export function editBase(data) {
  return fetch({
    url: '/es_service_create_base',
    method: 'post',
    data: data
  })
}


// 获取统计主题
export function getTjThem(index_name) {
  return fetch({
    url: '/es_service_get_tj_them',
    method: 'post',
    data: {name: index_name}
  })
}

// 编辑统计主题
export function editTjThem(data) {
  return fetch({
    url: '/es_service_edit_tj_them',
    method: 'post',
    data: data
  })
}
///////////////////////////////////////////////////////////////////////////////
// 获取主索引信息（枚举信息，字段状态信息）
export function getEsdata() {
  return fetch({
    url: '/es_service_get_esdata',
    method: 'get'
  })
}

// 生成子表结构
export function addEsdata(data) {
  return fetch({
    url: '/es_service_add_esdata',
    method: 'post',
    data: data
  })
}

// 填表
export function updateEsdata(data) {
  return fetch({
    url: '/es_service_add_form_data',
    method: 'post',
    data: data
  })
}

// 获取已填写的表数据
export function fullTable(name){
  return fetch({
    url: '/es_service_get_form_data',
    method: 'post',
    data: {name:name}
  })
}

// 删除
export function deleEsdata(data) {
  return fetch({
    url: '/es_service_delete_es',
    method: 'post',
    data: data
  })
}

// 根据统计主题做统计
export function getSbTj(data) {
  return fetch({
    url: '/es_service_sum_form_data',
    method: 'post',
    data: data
  })
}
import fetch from '@/utils/fetch'

// 获取导航菜单信息
export function getRoutes(orgName){
  return fetch({
    url: '/user_route',
    method: 'post',
    data: {name:orgName}
  })
}

// 获取角色信息
export function getRoles(orgName){
  return fetch({
    url: '/get_route',
    method: 'post',
    data: {name:orgName}
  })
}



















export function api_hand_optimized(type, data) {
  return fetch({
    url: '/route/optimized/hand',
    method: 'post',
    data: {
      type,
      data,
    }
  })
}

export function api_semi_optimized(type, data) {
  return fetch({
    url: '/route/optimized/semi',
    method: 'post',
    data: {
      type,
      data,
    }
  })
}

export function api_load_hand_log(offset, limit, router_line) {
  return fetch({
    url: '/route/log/hand',
    method: 'post',
    data: {
      limit,
      offset,
      router_line
    }
  })
}

export function api_load_semi_log(offset, limit) {
  return fetch({
    url: '/route/log/semi',
    method: 'post',
    data: {
      limit,
      offset,
    }
  })
}

export function api_switch_line(line_id, line_name, ip_section) {
  return fetch({
    url: '/route/line/switch',
    method: 'post',
    data: {
      line_id,
      line_name,
      ip_section,
    }
  })
}

export function api_search_ip_section(type, data) {
  return fetch({
    url: '/route/search/ip_section',
    method: 'post',
    data: {
      type,
      data,
    }
  })
}

export function api_load_line(depth) {
  console.log("api_load_line");
  return fetch({
    url: '/route/conf/load_load_line_conf_by_tree',
    method: 'post',
    data: {
      type: "load_line_conf_by_tree",
      data: {
        type: "load_line_conf_by_tree",
        depth: depth
      }
    }
  })
}

export function api_load_region() {
  return fetch({
    url: '/route/load/region',
    method: 'get'
  })
}


export function apiAddRouter(type, is_restart, data) {
  return fetch({
    url: '/route/addRouter',
    method: 'post',
    data: {
      type,
      is_restart,
      data
    }
  })
}

export function apiAddRoom(type, data) {
  return fetch({
    url: '/route/addRoom',
    method: 'post',
    data: {
      type,
      data
    }

  })
}

export function getRouter(type, data) {
  return fetch({
    url: '/route/getRouter',
    method: 'post',
    data: {
      type,
      data
    }
  })
}

export function ApiDelRouter(type, id) {
  return fetch({
    url: '/route/delRouter',
    method: 'post',
    data: {
      type: "del_router",
      data: {type: type, id: id}
    }

  })
}


export function restartExaBgp(data) {
  return fetch({
    url: '/route/config/exabgp/restart',
    method: 'post',
    data: data
  })
}

export function loadRoom(type) {
  return fetch({
    url: '/route/loadRoom',
    method: 'post',
    data: {
      type
    }
  })
}

export function apiDelRoom(type, id) {
  return fetch({
    url: '/route/delRoom',
    method: 'post',
    data: {
      type,
      data: {
        id: id
      }
    }
  })
}

export function apiDelCommunity(type, id) {
  return fetch({
    url: '/route/delCommunity',
    method: 'post',
    data: {
      type,
      data: {
        id: id
      }
    }
  })
}

export function loadCommunity(type, peer_id) {
  return fetch({
    url: '/route/loadCommunity',
    method: 'post',
    data: {
      type,
      peer_id
    }
  })
}

export function ApiAddCommunity(type, data) {
  return fetch({
    url: '/route/addCommunity',
    method: 'post',
    data: {
      type,
      data
    }
  })
}

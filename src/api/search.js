import fetch from '@/utils/fetch'


export function create_service(searchData) {
  return fetch({
    url: '/search_service_searchsuggest?data='+searchData,
    method: 'get',
  })
}

//
export function source(searchData) {
  return fetch({
    url: '/search_service_source?data='+searchData,
    method: 'get',
  })
}

import fetch from '@/utils/fetch'



// 人才库展示
export function getpepData() {
    return fetch({
        url: '/pep_service_get_pepdata',
        method: 'get',
    })
}

//  编辑人才
export function getpepMapp(id,form) {
    return fetch({
        url: '/pep_service_set_pepoles',
        method: 'post',
        data: {
          id,form
        }
    })
}

// 获取人才数据
export function getpepInfos(data) {
    return fetch({
        url: '/pep_service_get_pepoles',
        method: 'post',
        data: data
    })
}

// 添加数据
export function addpepdata(data) {
    return fetch({
        url: '/pep_service_add_pepoles',
        method: 'post',
        data: data
    })
}


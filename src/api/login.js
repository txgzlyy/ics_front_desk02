import fetch from '@/utils/fetch'

export function login(username, password) {
  return fetch({
    url: '/db_service_login',
    method: 'post',
    data: {
      username,
      password,
    }
  })
}

export function getInfo(token) {
  return fetch({
    url: '/db_service_user_info',
    method: 'get'
  })
}

export function logout() {
  return fetch({
    url: '/db_service_logout',
    method: 'post'
  })
}

export function newUsers(username, password) {
  return fetch({
    url: '/db_service_users',
    method: 'post',
    data: {
      username,
      password,
    }
  })
}

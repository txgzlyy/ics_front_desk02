import fetch from '@/utils/fetch'

export function autoOps(data) {
  return fetch({
    url: '/ansible/auto_ops',
    method: 'post',
    data: data,
    timeout: 1800000
  })
}

export function getPlaybook(data) {
  return fetch({
    url: '/ansible/ansible',
    method: 'get',
    timeout: 1800000
  })
}

export function executePlaybook(data) {
  return fetch({
    url: '/ansible/ansible',
    method: 'post',
    data: data,
    timeout: 1800000
  })
}

export function executePlaybooks(data) {
  return fetch({
    url: '/ansible/playbooks',
    method: 'post',
    data: data,
    timeout: 1800000
  })
}

export function uploadFile(data) {
  return fetch({
    url: '/ansible/upload',
    method: 'post',
    file: data,
    data: data,
    timeout: 1800000
  })
}

export function savePlaybookTemplate(data) {
  return fetch({
    url: '/ansible/playbook_template',
    method: 'post',
    data: data,
    timeout: 150000
  })
}

export function getPlaybookTemplate(data) {
  return fetch({
    url: '/ansible/playbook_template',
    method: 'get',
    timeout: 150000
  })
}

export function deletePlaybookTemplate(data) {
  return fetch({
    url: '/ansible/playbook_template',
    method: 'delete',
    data: data,
    timeout: 150000
  })
}
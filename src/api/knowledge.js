import fetch from '@/utils/fetch'



// 知识库列表展示
export function getlistData() {
  return fetch({
    url: '/Knowledge_service_show',
    method: 'get',
  })
}

//  编辑人才
export function editContent(data) {
  return fetch({
    url: '/Knowledge_service_edit_content',
    method: 'post',
    data: {
      data
    }
  })
}


export function update_content(data) {
  return fetch({
    url: '/Knowledge_service_update_content',
    method: 'post',
    data: {
      data
    }
  })
}


// 获取历史列表
export function version_list(data) {
  return fetch({
    url: '/Knowledge_service_version_list',
    method: 'post',
    data: {data}
  })
}




// 添加/修改数据
export function add_knowledge(data) {
  return fetch({
    url: '/Knowledge_service_create',
    method: 'post',
    data: data
  })
}



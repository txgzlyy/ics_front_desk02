import fetch from '@/utils/fetch'


// 自定义自由表格
export function createBase(data) {
  return fetch({
    url: '/assets_service_create_base',
    method: 'post',
    data: data
  })
}

// 修改自由表格
export function editBase(data) {
  return fetch({
    url: '/assets_service_create_base',
    method: 'post',
    data: data
  })
}


///////////////////////////////////////////////////////////////////////////////
// 获取主索引信息（枚举信息，字段状态信息）
export function getEsdata() {
  return fetch({
    url: '/assets_service_get_esdata',
    method: 'get'
  })
}

// 生成子表结构
export function addEsdata(data) {
  return fetch({
    url: '/assets_service_add_esdata',
    method: 'post',
    data: data
  })
}

// 填表
export function updateEsdata(data) {
  return fetch({
    url: '/assets_service_add_form_data',
    method: 'post',
    data: data
  })
}

// 获取已填写的表数据
export function fullTable(name,fNum){
  return fetch({
    url: '/assets_service_get_form_data',
    method: 'post',
    data: {name:name,from:fNum}
  })
}

// 获取某系统的所有设备
export function fullSys(name,sys,fNum){
  return fetch({
    url: '/assets_service_get_form_data',
    method: 'post',
    data: {name:name,sys:sys,from:fNum}
  })
}
// 获取地区
export function Areas(){
  return fetch({
    url: '/assets_service_get_areas',
    method: 'get',
  })
}

// 删除
export function deleEsdata(data) {
  return fetch({
    url: '/assets_service_delete_es',
    method: 'post',
    data: data
  })
}

// 上传excel
export function upLoadExe(data) {
  return fetch({
    url: '/upload',
    method: 'post',
    data: data,
    headers: { 
      'Content-Type': 'multipart/form-data'
    }

  })
}

// 数据导出excel
export function outExcel(data) {
  return fetch({
    url: '/assets_service_out_excel',
    method: 'post',
    data: data
  })
}


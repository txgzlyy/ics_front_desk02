import fetch from '@/utils/fetch'

// 创建组织
export function createOrg(data) {
  return fetch({
    url: '/db_service_create_organ',
    method: 'post',
    data: data
  })
}

// 获取组织
export function getOrg() {
  return fetch({
    url: '/db_service_query_organ',
    method: 'get'
  })
}

// 获取创建的组织
export function getcOrg() {
  return fetch({
    url: '/db_service_query_corgan',
    method: 'get'
  })
}

// 获取加入的组织
export function getjOrg() {
  return fetch({
    url: '/db_service_query_jorgan',
    method: 'get'
  })
}

// 获取应用
export function getApp() {
  return fetch({
    url: '/db_service_query_app',
    method: 'get'
  })
}

// 用户加入组织
export function userOrg(data) {
  return fetch({
    url: '/db_service_jion_org',
    method: 'post',
    data: data
  })
}

// 获取加入的组织下的所有应用
export function getOrgApp(data) {
  return fetch({
    url: '/db_service_org_app',
    method: 'post',
    data: data
  })
}

// 获取组织申请消息
export function getOrgMsg() {
  return fetch({
    url: '/db_service_get_orgmsg',
    method: 'get',
  })
}

// 处理组织申请消息
export function handleOrgApply(data) {
  return fetch({
    url: '/db_service_hand_orgapply',
    method: 'post',
    data: data
  })
}

// 获取角色申请消息
export function getRoleMsg() {
  return fetch({
    url: '/db_service_get_rolemsg',
    method: 'get',
  })
}

// 处理角色申请消息
export function handleRoleApply(data) {
  return fetch({
    url: '/db_service_hand_roleapply',
    method: 'post',
    data: data
  })
}


// 处理申请应用
export function handleApplyApp(data) {
  return fetch({
    url: '/db_service_input_role_permission',
    method: 'post',
    data: data
  })
}

// 注册

export function UserRegist(data) {
  return fetch({
    url: '/db_service_regist_user',
    method: 'post',
    data: data
  })
}

export function getAppId() {
  return fetch({
    url: '/db_service_apply_appid',
    method: 'get'
  })
}
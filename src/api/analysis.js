import fetch from '@/utils/fetch'

// 获取已填写的表数据
export function fullTjCla(data) {
  return fetch({
    url: '/analysis_service_assete_tjcla_data',
    method: 'post',
    data: data
  })
}

// 获取分类设备的信息
export function ClaAssetInfo(data) {
  return fetch({
    url: '/analysis_service_assete_tj_infos',
    method: 'post',
    data: data
  })
}


// 获取企业分类统计
export function enterTjCla(data) {
  return fetch({
    url: '/analysis_service_enter_tjcla_data',
    method: 'post',
    data: data
  })
}
// 获取分类企业的信息
export function ClaEnterInfo(data) {
  return fetch({
    url: '/analysis_service_enter_tj_infos',
    method: 'post',
    data: data
  })
}

// 添加地区权限申请
export function applyAreaPermis(data) {
  return fetch({
    url: '/analysis_service_apply_area_permis',
    method: 'post',
    data: data
  })
}

// 获取地区权限申请
export function getAreaPermis(data) {
  return fetch({
    url: '/analysis_service_get_area_permis',
    method: 'post',
    data: data
  })
}

// 处理地区权限申请
export function handleAreaApply(data) {
  return fetch({
    url: '/analysis_service_handle_area_apply',
    method: 'post',
    data: data
  })
}

// 获取地区
export function Areas(){
  return fetch({
    url: '/analysis_service_get_areas',
    method: 'get',
  })
}

// 数据导出excel
export function outExcel(data) {
  return fetch({
    url: '/analysis_service_out_excel',
    method: 'post',
    data: data
  })
}

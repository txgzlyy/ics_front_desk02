export function notice(me, data1, msg) {

  if (data1.code > 0) {

    me.fullscreenLoading = false

    if (msg === undefined || msg === '') {
      msg = data1.desc
    }

    me.$notify({
      title: '成功',
      message: msg,
      type: 'success'
    });
  } else {
    me.$notify.error({
      title: '错误',
      message: data1.desc
    });
  }
}

export function confirm4delete(me, func) {
  me.$confirm('删除记录, 是否继续', '警告', {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    type: 'warning'
  }).then(() => {
    func()
  }).catch(() => {
    me.$message({
      type: 'info',
      message: '已取消删除'
    });
  })
}

export function removeElementById(me, list, id) {
  let remove = list.some(function (value, i) {
    if (value.id === id) {
      list.splice(i, 1);
      me.fullscreenLoading = false
      return true
    }
  })

  if (remove) {
    me.$message({
      type: 'info',
      message: '删除成功'
    });
  } else {
    me.fullscreenLoading = false
    me.$message({
      type: 'warning',
      message: '未找到记录'
    });
  }
}

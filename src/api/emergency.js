import fetch from '@/utils/fetch'

// ----------------------------------emergency fetch--------------------------
export function push_emergency(data) {
  return fetch({
    url: '/emergency_add_emergency',
    method: 'post',
    data: {data: data}
  })
}


export function get_emergencies() {
  return fetch({
    url: '/emergency_get_emergencies',
    method: 'get',
    // params:{id:id}
  })
}

export function get_emergency(id) {
  return fetch({
    url: '/emergency_get_emergency',
    method: 'get',
    params: {id: id}
  })
}

export function update_emergency(id, data) {
  return fetch({
    url: '/emergency_update_emergency',
    method: 'put',
    data: {id: id, data: data}
  })
}

export function delete_emergency(id) {
  return fetch({
    url: '/emergency_delete_emergency',
    method: 'delete',
    data: {id: id}
  })
}

// ----------------------------------director fetch--------------------------
export function add_director(data) {
  return fetch({
    url: '/director_add_director',
    method: 'post',
    data: {data: data}
  })
}


export function save_director(id, data) {
  return fetch({
    url: '/director_save_director',
    method: 'put',
    data: {id: id, data: data}
  })
}

export function exec_step(data) {
  return fetch({
    url: '/director_exec_step',
    method: 'put',
    data: {data: data}
  })
}

export function get_director(id) {
  return fetch({
    url: '/director_get_director',
    method: 'get',
    params: {id: id}
  })
}

export function delete_director(id) {
  return fetch({
    url: '/director_delete_director',
    method: 'delete',
    data: {id: id}
  })
}

export function get_directors() {
  return fetch({
    url: '/director_get_directors',
    method: 'get'
  })

}

// -----------------------------others--------------------------
export function uuid() {
  var s = [];
  var hexDigits = "0123456789abcdef";
  for (var i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
  }
  s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
  s[8] = s[13] = s[18] = s[23] = "-";

  var uuid = s.join("");
  return uuid;
}

export class Node {
  constructor(title, desc, principal, content) {
    this.id = uuid();
    this.content = content;
    this.isHead = false;
    this.title = title;
    this.name = title
    this.desc = desc;
    this.principal = principal;
    this.children = [];
    this.state = 'todo'
    console.log(this.id);
  }

  print() {
    console.log(this.id + this.principal + this.content)
  }

  set_pid(pid) {
    this.pid = pid;
  }

  set_head(isHead) {
    this.isHead = isHead;
  }

  set_record(info) {
    this.info = info
  }

  set_start_time(time) {
    this.start_time = time
  }

  set_end_time(time) {
    this.end_time = time
  }

  set_principal(principal) {
    this.principal = principal
  }

  o2j() {
    // return {
    //   id:this.id,
    //   principal:this.principal,
    //   content:this.content,
    //   isHead:this.isHead,
    //   title:this.title,
    //   desc:this.desc,
    //   children:this.children
    // }
    return JSON.parse(JSON.stringify(this))
  }
}

export function data_str(){
    return new Date().toLocaleString('chinese', {hour12:false}).replace(/\//g, '-')
}

export function reviseTreeData(tree) {

  if (Array.isArray(tree)) {
    tree.forEach(function (value,i) {
      reviseTreeData(value)
    })

    return
  }

  if (tree.children.length > 0) {
    let children = tree['children'];

    children.forEach(function (value, i) {
      reviseTreeData(value)
    });
  }

  tree['name'] = tree['title']
}

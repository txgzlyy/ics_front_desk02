import fetch from '@/utils/fetch'

// push data to server
export function add_lab(data) {
  return fetch({
    url: '/labs_add',
    method: 'post',
    data: {data: data}
  })
}


export function update_lab(id, data) {
  return fetch({
    url: '/labs_update',
    method: 'put',
    data: {id: id, data: data}
  })
}


// Gains data from server
export function get_labs() {
  return fetch({
    url: '/labs_get_labs',
    method: 'get'
  })
}
// Gains data from server
export function get_matched_instance() {
  return fetch({
    url: '/labs_get_origin_labs',
    method: 'get'
  })
}

export function get_lab(id) {
  return fetch({
    url: '/labs_get_lab',
    method: 'get',
    params: {id: id}
  })
}

export function delete_lab(id) {
  return fetch({
    url:'/labs_delete',
    method:'delete',
    data:{id:id}
  })
}

// ------------------------------------lab pool----------------------------

// push data to server
export function add_pool(data) {
  return fetch({
    url: '/lab_pool_add',
    method: 'post',
    data: {data: data}
  })
}


export function update_pool(id, data) {
  return fetch({
    url: '/lab_pool_update',
    method: 'put',
    data: {id: id, data: data}
  })
}


// Gains data from server
export function get_pools() {
  return fetch({
    url: '/lab_pool_get_pools',
    method: 'get'
  })
}

export function get_pool(id) {
  return fetch({
    url: '/lab_pool_get_pool',
    method: 'get',
    params: {id: id}
  })
}

export function delete_pool(id) {
  return fetch({
    url:'/lab_pool_delete',
    method:'delete',
    data:{id:id}
  })
}

// --------------------------------enter-----------------------
export function checkout(type) {
  return fetch({
    url: '/labs_checkout',
    method: 'get',
    params: {pool: type}
  })
}

export function get_vm_address(labId) {
  return fetch({
    url: '/labs_enter',
    method: 'get',
    params: {id: labId}
  })
}

export function exit_vm(labId) {
  return fetch({
    url: '/labs_exit',
    method: 'get',
    params: {id: labId}
  })
}


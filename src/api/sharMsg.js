import fetch from '@/utils/fetch'

// 获取所有主题
export function allTopic() {
  return fetch({
    url: '/sharinfo_service_topic_all_list',
    method: 'get'
  })
}

// 关注这个主题
export function follTopic(data) {
    return fetch({
      url: '/sharinfo_service_follow_topic',
      method: 'post',
      data: data
    })
}

// 查看有关这个主题的共享信息
export function lookSharMsg(data) {
    return fetch({
      url: '/sharinfo_service_shar_msg',
      method: 'post',
      data: data
    })
}

// 测试微服务添加共享信息，可删除
export function addTest(data) {
    return fetch({
      url: '/protest_service_add_sharing',
      method: 'post',
      data: data
    })
}